package com.cindodcindy.masakin.wekend;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.cindodcindy.masakin.R;

public class WeekendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekend);
    }
}